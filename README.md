# Software Studio 2019 Spring Assignment_02

# 作品網址：[https://105020021.gitlab.io/Assignment_02/]

## Topic
* Project Name : [Plane war]
* Key functions 
    1. Complete game process :
        start menu => game view => game over(show leaderboard and replay button)
    2. Basic rules :
        Player 、 enemies can move and shoot and their life will decrease when damaged.
        Map will move.
    3. Jucify mechanisms :
        If player get more score , the different enemies will be generated , the higher the score that player get
        the harder the game will be. 
        Player can press button 'Z' to use the skill to kill all the enemies and bullets in the map.
    4. Animations : Player 、 enemies have sprite animations.
    5. Particle : Show when player or enemy die.
    6. Sound 、 UI 、 Leaderboard : As you can see.
    
* Other functions 
    1. Off-line : 選擇'Start2P'玩雙人模式(雙人模式下沒有大招，強化物件也只有增加子彈)
    2. Enhanced items : 當玩家取的特定分數後，會有禮物出現在畫面中，玩家可以去觸碰獲得升級。
        禮物生成時會隨機從 自動瞄準 增加子彈 小幫手 中隨機選取一項(由於是隨機所以可能要多測試幾次~)
        ps. 自動瞄準 : 8秒內自動瞄準敵機(不含boss) 小幫手 : 出來時會有防護罩，過一段時間後消失
    3. Boss : 當玩家取得特定分數時會進入boss關，此時小怪將不會生成。
        總共有兩個王，兩個的攻擊方式都不一樣，打倒第二個王後遊戲結束。

    4. 怪風 : 過第一個boss才會生成，當玩家進入怪風領域當中移動速度會下降     

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|Y|
|Appearance|5%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|On-line|15%|N|
|Off-line|5%|Y|
|Enhanced items|15%|Y|
|Boss|5%|Y|



