var game = new Phaser.Game(500, 630, Phaser.AUTO, 'canvas');

game.States = {};

var move  = 5; // move speed
var score = 0;
var score2 = 0; // score for 2P
var playmode = 1;

game.States.boot = function() {
    this.preload = function() {
        game.load.image('loading', 'assets/preloader.gif');
    };

    this.create = function() {  
        game.state.start('load');
    };
};

game.States.load = function() {
    this.preload = function() {
        var preloadSprite = game.add.sprite(10, game.height/2, 'loading');
        game.load.setPreloadSprite(preloadSprite);
        game.load.image('title', 'assets/title.png');
        game.load.image('background', 'assets/Space.png');
        game.load.image('startbutton', 'assets/startbutton.png');
        game.load.image('startbutton2', 'assets/startbutton2.png');
        game.load.image('replaybutton', 'assets/replaybutton.png');
        game.load.spritesheet('player', 'assets/plane.png', 54.7, 60);
        game.load.spritesheet('player2', 'assets/plane2.png', 54.7, 60);
        game.load.spritesheet('enemy1', 'assets/enemy1.png', 52, 50);
        game.load.spritesheet('enemy2', 'assets/enemy2.png', 52, 50);
        game.load.spritesheet('enemy3', 'assets/enemy3.png', 52, 50);
        game.load.spritesheet('boss1' , 'assets/Boss1.png' , 129, 125);
        game.load.image('boss2', 'assets/Boss2.png');
        game.load.image('mybu', 'assets/mybullet.png');
        game.load.image('mybu2', 'assets/mybullet2.png');
        game.load.image('e1bu', 'assets/E1bullet.png');
        game.load.image('e2bu', 'assets/E2bullet.png');
        game.load.image('e3bu', 'assets/E3bullet.png');
        game.load.image('b1bu', 'assets/B1bullet.png');
        game.load.image('Pdie', 'assets/dieParticle(player).png');
        game.load.image('Edie', 'assets/dieParticle(enermy).png');
        game.load.image('B1die', 'assets/dieParticle(boss1).png');
        game.load.image('B2die', 'assets/dieParticle(boss2).png');
        game.load.image('gift', 'assets/gift.png');
        game.load.image('heart', 'assets/heart.png');
        game.load.image('helper', 'assets/helper.png');
        game.load.image('shield', 'assets/shield.png');
        game.load.image('wind', 'assets/wind.png');
        game.load.image('menu', 'assets/pausemenu.png');
        game.load.audio('shoot', 'assets/shoot.mp3');
        game.load.audio('bomb', 'assets/bomb.mp3');
        game.load.audio('siren', 'assets/siren.mp3');
        game.load.audio('levelup', 'assets/levelup.mp3');
        game.load.audio('damage', 'assets/damage.mp3');
        game.load.audio('die', 'assets/die.mp3');
        game.load.audio('bgm', 'assets/bgm.mp3');
        game.load.audio('bossbgm', 'assets/boss_bgm.mp3');
        game.load.audio('playbgm', 'assets/play_bgm.mp3');
        game.load.audio('bossdie', 'assets/bossdie.mp3');
    };

    this.update = function() {
        //check to see if the audio is decoded
        if (game.cache.isSoundDecoded('bgm')) {
            game.state.start("menu");
        }  
    };
};

game.States.menu = function() {
    this.create = function() {
        
        game.add.image(0, 0, 'title');
        // bgm
        this.bgm = game.add.audio('bgm', true);
        this.bgm.loopFull();
        // startbutton
        startbutton = game.add.button(game.width/2, 335, 'startbutton', this.oneP, this);
        startbutton.anchor.setTo(0.5, 0.5);
        startbutton2 = game.add.button(game.width/2, 410, 'startbutton2', this.twoP, this);
        startbutton2.anchor.setTo(0.5, 0.5);
    };
    // Click 'startbutton' to change the state
    this.oneP = function() {
        this.bgm.stop();
        playmode = 1;
        game.state.start('main');
    };

    this.twoP = function() {
        this.bgm.stop();
        playmode = 2;
        game.state.start('main');
    };
};

game.States.main = function() {
    this.create = function() {
        // background 
        this.bgm = game.add.audio('bgm', true);
        this.bg = game.add.tileSprite(0, 0, game.width, game.height, 'background');
        this.bg.autoScroll(0, 30);

        game.physics.startSystem(Phaser.Physics.ARCADE); // physics engine

        notice = game.add.text(game.width/2, 200, 'Press Space to pause , Z to use skill', {font: '24px Arial', fill: '#ffffff'});
        notice.anchor.setTo(0.5, 0.5);
        game.time.events.add(3000, function() {
            notice.kill();
        }, this);

        this.cursor = game.input.keyboard.createCursorKeys(); // up , down , right , left
        this.wasd = {
            up: game.input.keyboard.addKey(Phaser.Keyboard.W),
            down: game.input.keyboard.addKey(Phaser.Keyboard.S),
            left: game.input.keyboard.addKey(Phaser.Keyboard.A),
            right: game.input.keyboard.addKey(Phaser.Keyboard.D),
        }

        // skill
        if(playmode == 1) {
            this.skillnum = 3;
            this.skillkey = game.input.keyboard.addKey(Phaser.Keyboard.Z);
            this.skillkey.onDown.add(this.clean, this);
            this.skillLabel = game.add.text(300, 20, 'Skill:  0', {font: '18px Arial', fill: '#ffffff'});
            this.skillLabel.setText("Skill:  " + this.skillnum);
        }

        this.pausekey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.pausekey.onDown.add(this.pause, this); 

        // player
        if(playmode == 1) this.player = game.add.sprite(game.width/2, 300, 'player');
        else this.player = game.add.sprite(350, 300, 'player');
        this.player.anchor.setTo(0.5, 0.5);
        
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;
        
        game.add.image(422, 20, 'heart');
        this.player.level = 3; // initial level
        this.player.heart = 2;
        this.player.color = 1;
        this.player.die = false;
        this.lifeLabel = game.add.text(385, 20, 'Life:  0', {font: '18px Arial', fill: '#ffffff'});
        this.lifeLabel.setText("Life:  " + this.player.heart);
    
        this.player.animations.add('fly' , [1,2,3], 62, true);
        this.player.animations.add('flyl', [4,5], 62, true);
        this.player.animations.add('flyr', [6,7], 62, true);

        // player2
        if(playmode == 2) {
            this.player2 = game.add.sprite(150, 300, 'player2');
            this.player2.anchor.setTo(0.5, 0.5);
            
            game.physics.arcade.enable(this.player2);
            this.player2.body.collideWorldBounds = true;
            
            game.add.image(422, 55, 'heart');
            this.player2.level = 3; // initial level
            this.player2.heart = 2;
            this.player2.color = 2;
            this.player2.die = false;
            this.lifeLabel2 = game.add.text(385, 55, 'Life:  0   (2P)', {font: '18px Arial', fill: '#ffffff'});
            this.lifeLabel2.setText("Life:  " + this.player2.heart + "   (2P)");
        
            this.player2.animations.add('fly2' , [1,2,3], 62, true);
            this.player2.animations.add('flyl2', [4,5], 62, true);
            this.player2.animations.add('flyr2', [6,7], 62, true);
        }

        // score
        this.scoreLabel = game.add.text(30, 20, 'Score:  0', {font: '18px Arial', fill: '#ffffff'});
        if(playmode == 2) this.scoreLabel2 = game.add.text(30, 55, 'Score:  0   (2P)', {font: '18px Arial', fill: '#ffffff'});

        // gift
        this.gift_time = 1;
        this.gift_exist = false;
        this.gift = game.add.group();
        this.gift.enableBody = true;
        this.gift.createMultiple('1', 'gift');
        this.gift.setAll('checkWorldBounds', true);

        // enemy1
        this.enemy1 = game.add.group();
        this.enemy1.enableBody = true;
        this.enemy1.createMultiple('10', 'enemy1');
        this.enemy1.setAll('outOfBoundsKill', true);
        this.enemy1.setAll('checkWorldBounds', true);
        this.enemy1.callAll('animations.add',  'animations', 'enemy1fly', [0,1,2,3,4], 20, true);
        this.enemy1.callAll('animations.play', 'animations', 'enemy1fly');

        // enemy2
        this.enemy2 = game.add.group();
        this.enemy2.enableBody = true;
        this.enemy2.createMultiple('10', 'enemy2');
        this.enemy2.setAll('outOfBoundsKill', true);
        this.enemy2.setAll('checkWorldBounds', true);
        this.enemy2.callAll('animations.add',  'animations', 'enemy2fly', [0,1,2,3,4], 20, true);
        this.enemy2.callAll('animations.play', 'animations', 'enemy2fly');

        // enemy3
        this.enemy3 = game.add.group();
        this.enemy3.enableBody = true;
        this.enemy3.createMultiple('10', 'enemy3');
        this.enemy3.setAll('outOfBoundsKill', true);
        this.enemy3.setAll('checkWorldBounds', true);
        this.enemy3.callAll('animations.add',  'animations', 'enemy3fly', [0,1,2,3,4], 20, true);
        this.enemy3.callAll('animations.play', 'animations', 'enemy3fly');

        // boss1
        this.boss1 = game.add.sprite(game.width/2, -200, 'boss1');
        this.boss1.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.boss1);
        this.boss1.life = 300;
        this.boss1.animations.add('boss1fly' , [0,1,2,3,4], 20, true);
        this.boss1.animations.play('boss1fly');

        // boss2
        this.boss2 = game.add.sprite(game.width/2, -200, 'boss2');
        this.boss2.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.boss2);
        this.boss2.life = 350;

        // wind
        this.wind = game.add.group();
        this.wind.enableBody = true;
        this.wind.createMultiple('2', 'wind');
        this.wind.setAll('outOfBoundsKill', true);
        this.wind.setAll('checkWorldBounds', true);

        // helper
        this.helper = game.add.group();
        this.helper.enableBody = true;
        this.helper.createMultiple('12', 'helper');
        this.helper.setAll('outOfBoundsKill', true);
        this.helper.setAll('checkWorldBounds', true);

        // shield
        this.shield = game.add.group();
        this.shield.enableBody = true;
        this.shield.createMultiple('12', 'shield');

        // bullet
        this.mybu = game.add.group();
        this.mybu.enableBody = true;
        this.mybu.createMultiple('70', 'mybu');
        this.mybu.setAll('outOfBoundsKill', true);
        this.mybu.setAll('checkWorldBounds', true);

        // player2 bullet
        if(playmode == 2) {
            this.mybu2 = game.add.group();
            this.mybu2.enableBody = true;
            this.mybu2.createMultiple('70', 'mybu2');
            this.mybu2.setAll('outOfBoundsKill', true);
            this.mybu2.setAll('checkWorldBounds', true);
        }

        // enemy1 bullet
        this.e1bu = game.add.group();
        this.e1bu.enableBody = true;
        this.e1bu.createMultiple('20', 'e1bu');
        this.e1bu.setAll('outOfBoundsKill', true);
        this.e1bu.setAll('checkWorldBounds', true);

        // enemy2 bullet
        this.e2bu = game.add.group();
        this.e2bu.enableBody = true;
        this.e2bu.createMultiple('60', 'e2bu');
        this.e2bu.setAll('outOfBoundsKill', true);
        this.e2bu.setAll('checkWorldBounds', true);

        // enemy3 bullet
        this.e3bu = game.add.group();
        this.e3bu.enableBody = true;
        this.e3bu.createMultiple('15', 'e3bu');
        this.e3bu.setAll('outOfBoundsKill', true);
        this.e3bu.setAll('checkWorldBounds', true);

        // boss1 bullet
        this.b1bu = game.add.group();
        this.b1bu.enableBody = true;
        this.b1bu.createMultiple('20', 'b1bu');
        this.b1bu.setAll('outOfBoundsKill', true);
        this.b1bu.setAll('checkWorldBounds', true);

        // generate bullet and enemy 
        this.genBullet = game.time.events.loop(200, this.Fire, this);
        if(playmode == 2) this.genBullet2 = game.time.events.loop(200, this.Fire2, this);
        this.genE1 = game.time.events.loop(1500, this.addEnemy1, this);
        this.genE2 = game.time.events.loop(3000, this.addEnemy2, this);
        this.genE3 = game.time.events.loop(2500, this.addEnemy3, this);
        this.genwind = game.time.events.loop(12000, this.addwind, this);

        // generate boss1
        this.boss1_exist = false;
        this.boss1_die = false;
        this.boss1_come = game.add.tween(this.boss1).to({y: 100}, 5000);
        this.boss1_move = game.add.tween(this.boss1).to({x: 80}, 2500)
                                                   .to({x: 420}, 5000)
                                                   .to({x: game.width/2}, 2500).loop();
        game.time.events.loop(1200, this.boss1Fire, this);

        // generate boss2
        this.boss2_exist = false;
        this.boss2_die = false;
        this.boss2_come = game.add.tween(this.boss2).to({y: 100}, 5000);
        this.boss2_move = game.add.tween(this.boss2).to({x: 80}, 2500)
                                                   .to({x: 420}, 5000)
                                                   .to({x: game.width/2}, 2500).loop();
        game.time.events.loop(300, this.boss2Fire, this);

        // dieParticle(player)
        this.Pdie_emitter = game.add.emitter(0, 0, 8);
        this.Pdie_emitter.makeParticles('Pdie');
        this.Pdie_emitter.setYSpeed(-150, 150);
        this.Pdie_emitter.setXSpeed(-150, 150); 
        this.Pdie_emitter.setScale(1.5, 0, 1.5, 0, 1500);

        // dieParticle(enermy)
        this.Edie_emitter = game.add.emitter(0, 0, 20);
        this.Edie_emitter.makeParticles('Edie');
        this.Edie_emitter.setYSpeed(-150, 150);
        this.Edie_emitter.setXSpeed(-150, 150); 
        this.Edie_emitter.setScale(1.5, 0, 1.5, 0, 500);

        // dieParticle(boss1)
        this.B1die_emitter = game.add.emitter(0, 0, 25);
        this.B1die_emitter.makeParticles('B1die');
        this.B1die_emitter.setYSpeed(-250, 250);
        this.B1die_emitter.setXSpeed(-250, 250); 
        this.B1die_emitter.setScale(1, 0, 1, 0, 4000);

        // dieParticle(boss2)
        this.B2die_emitter = game.add.emitter(0, 0, 25);
        this.B2die_emitter.makeParticles('B2die');
        this.B2die_emitter.setYSpeed(-250, 250);
        this.B2die_emitter.setXSpeed(-250, 250); 
        this.B2die_emitter.setScale(1, 0, 1, 0, 4000);

        // sound
        this.shootsound = game.add.audio('shoot', 0.2);
        this.bombsound = game.add.audio('bomb');
        this.sirensound = game.add.audio('siren');
        this.levelupsound = game.add.audio('levelup');
        this.diesound = game.add.audio('die');
        this.damagedsound = game.add.audio('damage');
        this.playbgm = game.add.audio('playbgm', true);
        this.bossbgm = game.add.audio('bossbgm', true);
        this.bossdie = game.add.audio('bossdie', true);
        this.playbgm.loopFull();
    };

    this.update = function() {

        this.movePlayer();
        this.enemy1Fire();
        this.enemy2Fire();
        this.enemy3Fire();
        this.helperFire();
        this.unprotect();

        game.physics.arcade.overlap(this.mybu, this.enemy1, this.enemy1Die, null, this);
        game.physics.arcade.overlap(this.mybu, this.enemy2, this.enemy2Die, null, this);
        game.physics.arcade.overlap(this.mybu, this.enemy3, this.enemy3Die, null, this);
        game.physics.arcade.overlap(this.mybu, this.boss1, this.boss1_damaged, null, this);
        game.physics.arcade.overlap(this.mybu, this.boss2, this.boss2_damaged, null, this);

        game.physics.arcade.overlap(this.e1bu, this.player, this.damaged, null, this);
        game.physics.arcade.overlap(this.e2bu, this.player, this.damaged, null, this);
        game.physics.arcade.overlap(this.e3bu, this.player, this.damaged, null, this);
        game.physics.arcade.overlap(this.b1bu, this.player, this.damaged, null, this);
        game.physics.arcade.overlap(this.e1bu, this.helper, this.helperDie, null, this);
        game.physics.arcade.overlap(this.e2bu, this.helper, this.helperDie, null, this);
        game.physics.arcade.overlap(this.e3bu, this.helper, this.helperDie, null, this);
        game.physics.arcade.overlap(this.b1bu, this.helper, this.helperDie, null, this);
        game.physics.arcade.overlap(this.e1bu, this.shield, this.protect, null, this);
        game.physics.arcade.overlap(this.e2bu, this.shield, this.protect, null, this);
        game.physics.arcade.overlap(this.e3bu, this.shield, this.protect, null, this);
        game.physics.arcade.overlap(this.b1bu, this.shield, this.protect, null, this);

        game.physics.arcade.overlap(this.enemy1, this.player, this.Crash, null, this);
        game.physics.arcade.overlap(this.enemy2, this.player, this.Crash, null, this);
        game.physics.arcade.overlap(this.enemy3, this.player, this.Crash, null, this);
        game.physics.arcade.overlap(this.boss1, this.player, this.Crash, null, this);
        game.physics.arcade.overlap(this.boss2, this.player, this.Crash, null, this);
        game.physics.arcade.overlap(this.enemy1, this.helper, this.helperCrash, null, this);
        game.physics.arcade.overlap(this.enemy2, this.helper, this.helperCrash, null, this);
        game.physics.arcade.overlap(this.enemy3, this.helper, this.helperCrash, null, this);
        game.physics.arcade.overlap(this.boss1, this.helper, this.helperCrash, null, this);
        game.physics.arcade.overlap(this.boss2, this.helper, this.helperCrash, null, this);
        game.physics.arcade.overlap(this.enemy1, this.shield, this.enemy1Die, null, this);
        game.physics.arcade.overlap(this.enemy2, this.shield, this.enemy2Die, null, this);
        game.physics.arcade.overlap(this.enemy3, this.shield, this.enemy3Die, null, this);

        this.inwind = false;
        game.physics.arcade.overlap(this.gift, this.player, this.levelup, null, this);
        game.physics.arcade.overlap(this.wind, this.player, this.slow, null, this);

        if(playmode == 1) {
            if(score > 180 * this.gift_time && this.gift_exist == false) {
                this.addgift();
                this.gift_time  = this.gift_time + 2;
                this.gift_exist = true;
            }
    
            if(score > 400 && this.boss1_exist == false && this.boss1_die == false) {
                this.boss1_exist = true;
                game.camera.shake(0.008, 3500);
                this.boss1_come.start();
                this.boss1_move.start();
                this.sirensound.fadeIn(3500);
                this.playbgm.stop();
                this.bossbgm.play();
            }
            
            if(score > 1200 && this.boss2_exist == false && this.boss2_die == false) {
                this.boss2_exist = true;
                game.camera.shake(0.008, 3500);
                this.boss2_come.start();
                this.boss2_move.start();
                this.sirensound.fadeIn(3500);
                this.playbgm.stop();
                this.bossbgm.play();
            }
        }

        if(playmode == 2) {
            game.physics.arcade.overlap(this.mybu2, this.enemy1, this.enemy1Die, null, this);
            game.physics.arcade.overlap(this.mybu2, this.enemy2, this.enemy2Die, null, this);
            game.physics.arcade.overlap(this.mybu2, this.enemy3, this.enemy3Die, null, this);
            game.physics.arcade.overlap(this.mybu2, this.boss1, this.boss1_damaged, null, this);
            game.physics.arcade.overlap(this.mybu2, this.boss2, this.boss2_damaged, null, this);

            game.physics.arcade.overlap(this.e1bu, this.player2, this.damaged, null, this);
            game.physics.arcade.overlap(this.e2bu, this.player2, this.damaged, null, this);
            game.physics.arcade.overlap(this.e3bu, this.player2, this.damaged, null, this);
            game.physics.arcade.overlap(this.b1bu, this.player2, this.damaged, null, this);

            game.physics.arcade.overlap(this.enemy1, this.player2, this.Crash, null, this);
            game.physics.arcade.overlap(this.enemy2, this.player2, this.Crash, null, this);
            game.physics.arcade.overlap(this.enemy3, this.player2, this.Crash, null, this);
            game.physics.arcade.overlap(this.boss1, this.player2, this.Crash, null, this);
            game.physics.arcade.overlap(this.boss2, this.player2, this.Crash, null, this);

            game.physics.arcade.overlap(this.gift, this.player2, this.levelup, null, this);
            game.physics.arcade.overlap(this.wind, this.player2, this.slow, null, this);

            if(score + score2 > 180 * this.gift_time && this.gift_exist == false) {
                this.addgift();
                this.gift_time  = this.gift_time + 2;
                this.gift_exist = true;
            }

            if(score + score2 > 400 && this.boss1_exist == false && this.boss1_die == false) {
                this.boss1_exist = true;
                game.camera.shake(0.008, 3500);
                this.boss1_come.start();
                this.boss1_move.start();
                this.sirensound.fadeIn(3500);
                this.playbgm.stop();
                this.bossbgm.play();
            }  

            if(score + score2 > 1200 && this.boss2_exist == false && this.boss2_die == false) {
                this.boss2_exist = true;
                game.camera.shake(0.008, 3500);
                this.boss2_come.start();
                this.boss2_move.start();
                this.sirensound.fadeIn(3500);
                this.playbgm.stop();
                this.bossbgm.play();
            }
        }
    };

    this.damaged = function(player, bullet) {

        bullet.kill();
        if(player.heart == 1){
            player.heart = 0;
            this.playerDie();
        } else {
            this.damagedsound.play();
            game.camera.flash(0xff0000, 800);
            player.level --;
            player.heart --;
        }
        if(player.color == 1) this.lifeLabel.setText("Life:  " + this.player.heart);
        else this.lifeLabel2.setText("Life:  " + this.player2.heart + "   (2P)");
    };

    this.boss1_damaged = function(boss, bullet) {

        bullet.kill();
        if(boss.life == 0){
            boss.kill();
            this.boss1_exist = false;
            this.boss1_die = true;

            this.B1die_emitter.x = boss.x;
            this.B1die_emitter.y = boss.y;
            this.B1die_emitter.start(true, 4000, null, 25);

            this.bossbgm.stop();
            this.bossdie.play();
            this.playbgm.loopFull();

            this.bg.autoScroll(0, 60);

            if(this.player.die == false) {
                score += 100;
                this.scoreLabel.setText("Score: " + score);
            }
            if(playmode == 2 && this.player2.die == false) {
                score2 += 100;
                this.scoreLabel2.setText("Score: " + score2 + "  (2P)");
            }
        } else boss.life --;
    };

    this.boss2_damaged = function(boss, bullet) {

        bullet.kill();
        if(boss.life == 0){
            boss.kill();
            this.boss2_exist = false;
            this.boss2_die = true;

            this.B2die_emitter.x = boss.x;
            this.B2die_emitter.y = boss.y;
            this.B2die_emitter.start(true, 4000, null, 25);

            this.bossbgm.stop();
            this.bossdie.play();
            this.bgm.loopFull();

            youwin = game.add.text(game.width/2, 250, 'WOW ! You Win !!!', {font: '32px Arial', fill: '#ffffff'});
            youwin.anchor.setTo(0.5, 0.5);

            exit = game.add.text(game.width/2, 320, 'Press Q to exit', {font: '24px Arial', fill: '#ffffff'});
            exit.anchor.setTo(0.5, 0.5);

            this.exitkey = game.input.keyboard.addKey(Phaser.Keyboard.Q);
            this.exitkey.onDown.add(this.exit, this); 

            this.bg.autoScroll(0, 120);
            game.time.events.remove(this.genBullet);
            if(playmode == 2) game.time.events.remove(this.genBullet2);

            if(this.player.die == false) {
                score += 1000;
                this.scoreLabel.setText("Score: " + score);
            }
            if(playmode == 2 && this.player2.die == false) {
                score2 += 1000;
                this.scoreLabel2.setText("Score: " + score2 + "  (2P)");
            }

        } else boss.life --;
    };

    this.playerDie = function() {

        this.diesound.play(); 
        game.camera.flash(0xff0000, 800);

        if(this.player.heart == 0 && this.player.die == false) {
            
            game.time.events.remove(this.genBullet);
            this.Pdie_emitter.x = this.player.x;
            this.Pdie_emitter.y = this.player.y;
            this.Pdie_emitter.start(true, 1500, null, 5);
            this.player.kill();
            this.player.die = true;
        }

        if(playmode == 2 && this.player2.heart == 0 && this.player2.die == false) {

            game.time.events.remove(this.genBullet2);
            this.Pdie_emitter.x = this.player2.x;
            this.Pdie_emitter.y = this.player2.y;
            this.Pdie_emitter.start(true, 1500, null, 5);
            this.player2.kill();       
            this.player2.die = true;
        }

        if(playmode == 1) {
            game.time.events.add(2000, function() {
                if(this.boss1_exist == true || this.boss2_exist == true) this.bossbgm.stop();
                else this.playbgm.stop();     
                game.state.start('over');
            }, this);
        }

        if(playmode == 2 && this.player.heart == 0 && this.player2.heart == 0){
            game.time.events.add(2000, function() {
                if(this.boss1_exist == true || this.boss2_exist == true) this.bossbgm.stop();
                else this.playbgm.stop();     
                game.state.start('over2');
            }, this);
        }
    };

    this.Crash = function(player, enemy) {

        this.diesound.play(); 
        game.camera.flash(0xff0000, 800);

        this.Pdie_emitter.x = player.x;
        this.Pdie_emitter.y = player.y;
        this.Pdie_emitter.start(true, 1500, null, 5);

        player.heart = 0;
        player.kill();

        if(player.color == 1 && player.die == false) {
            game.time.events.remove(this.genBullet); 
            this.lifeLabel.setText("Life:  0");
            player.die = true;
        }

        if(player.color == 2 && player.die == false) {
            game.time.events.remove(this.genBullet2);
            this.lifeLabel2.setText("Life:  0   (2P)");
            player.die = true;     
        }

        if(playmode == 1) {
            game.time.events.add(2000, function() {
                if(this.boss1_exist == true || this.boss2_exist == true) this.bossbgm.stop();
                else this.playbgm.stop();     
                game.state.start('over');
            }, this);
        }

        if(playmode == 2 && this.player.heart == 0 && this.player2.heart == 0){
            game.time.events.add(2000, function() {
                if(this.boss1_exist == true || this.boss2_exist == true) this.bossbgm.stop();
                else this.playbgm.stop();     
                game.state.start('over2');
            }, this);
        }
    };

    this.enemy1Die = function(mybu, enemy1) {
        
        this.Edie_emitter.x = enemy1.x;
        this.Edie_emitter.y = enemy1.y;
        this.Edie_emitter.start(true, 500, null, 2);
        this.bombsound.play();

        if(mybu.from == 1) {
            score += 10;
            this.scoreLabel.setText("Score:  " + score);
        } else if(playmode == 2 && mybu.from == 2) {
            score2 += 10;
            this.scoreLabel2.setText("Score:  " + score2 + "   (2P)");
        }

        mybu.kill();
        enemy1.kill();
    };

    this.enemy2Die = function(mybu, enemy2) {

        this.Edie_emitter.x = enemy2.x;
        this.Edie_emitter.y = enemy2.y;
        this.Edie_emitter.start(true, 500, null, 2);
        this.bombsound.play();

        if(mybu.from == 1) {
            score += 20;
            this.scoreLabel.setText("Score: " + score);
        } else if(playmode == 2 && mybu.from == 2) {
            score2 += 20;
            this.scoreLabel2.setText("Score:  " + score2 + "   (2P)");
        }

        mybu.kill();
        enemy2.kill();
    };

    this.enemy3Die = function(mybu, enemy3) {

        mybu.kill();
        if(enemy3.life == 0) {
            this.Edie_emitter.x = enemy3.x;
            this.Edie_emitter.y = enemy3.y;
            this.Edie_emitter.start(true, 500, null, 2);
            this.bombsound.play();

            if(mybu.from == 1) {
                score += 30;
                this.scoreLabel.setText("Score: " + score);
            } else if(playmode == 2 && mybu.from == 2) {
                score2 += 30;
                this.scoreLabel2.setText("Score:  " + score2 + "   (2P)");
            }
            enemy3.kill();
        } else {
            this.Edie_emitter.x = enemy3.x;
            this.Edie_emitter.y = enemy3.y;
            this.Edie_emitter.start(true, 300, null, 1);
            enemy3.life --;
        }
    }

    this.helperDie = function(helper, bullet) {
        this.Pdie_emitter.x = helper.x;
        this.Pdie_emitter.y = helper.y;
        this.Pdie_emitter.start(true, 500, null, 1);

        bullet.kill();
        helper.kill();
        if(helper.shield) helper.shield.kill(); 
    };

    this.helperCrash = function(enemy, helper) {
        this.Pdie_emitter.x = helper.x;
        this.Pdie_emitter.y = helper.y;
        this.Pdie_emitter.start(true, 500, null, 1);

        helper.kill(); 
    };

    this.protect = function(bullet, shield) {
        bullet.kill();
    };

    this.unprotect = function() {
        this.helper.forEachExists(function(helper) {
            if(helper.y < 320) helper.shield.kill();
        }, this);
    };

    this.movePlayer = function() {
        
        if(this.inwind == false) move = 5;
        else move = 1;

        if(this.cursor.left.isDown){
            this.player.x -= move;
            this.player.animations.play('flyl');
            if(this.cursor.up.isDown){
                this.player.y -= move;
            }
            if(this.cursor.down.isDown){
                this.player.y += move;
            }
        }
        else if(this.cursor.right.isDown){
            this.player.x += move;
            this.player.animations.play('flyr');
            if(this.cursor.up.isDown){
                this.player.y -= move;
            }
            if(this.cursor.down.isDown){
                this.player.y += move;
            }
        }
        else if(this.cursor.up.isDown){
            this.player.y -= move;
        } 
        else if(this.cursor.down.isDown){
            this.player.y += move;
        }  
        else {
            this.player.animations.play('fly');
        }

        if(playmode == 2) {
            if(this.wasd.left.isDown){
                this.player2.x -= move;
                this.player2.animations.play('flyl2');
                if(this.wasd.up.isDown){
                    this.player2.y -= move;
                }
                if(this.wasd.down.isDown){
                    this.player2.y += move;
                }
            }
            else if(this.wasd.right.isDown){
                this.player2.x += move;
                this.player2.animations.play('flyr2');
                if(this.wasd.up.isDown){
                    this.player2.y -= move;
                }
                if(this.wasd.down.isDown){
                    this.player2.y += move;
                }
            }
            else if(this.wasd.up.isDown){
                this.player2.y -= move;
            } 
            else if(this.wasd.down.isDown){
                this.player2.y += move;
            }  
            else {
                this.player2.animations.play('fly2');
            }
        }
    };

    this.addEnemy1 = function() {
        var enemy = this.enemy1.getFirstDead();
        if( (playmode == 1 && (score > 600 || this.boss1_exist == true || this.boss2_exist == true || this.boss2_die == true)) ||
            (playmode == 2 && (score + score2 > 600 || this.boss1_exist == true || this.boss2_exist == true || this.boss2_die == true))
        ) return;
        enemy.anchor.setTo(0.5, 1);
        enemy.reset(game.rnd.between(30, 470), 0);
        enemy.body.velocity.y = 70;
    };

    this.addEnemy2 = function() {
        var enemy = this.enemy2.getFirstDead();
        if( (playmode == 1 && (score < 100 || this.boss1_exist == true || this.boss2_exist == true || this.boss2_die == true)) ||
            (playmode == 2 && (score + score2 < 100 || this.boss1_exist == true || this.boss2_exist == true || this.boss2_die == true))
        ) return;
        enemy.anchor.setTo(0.5, 1);
        enemy.reset(game.rnd.between(30, 470), 0);
        enemy.body.velocity.y = 70;
    };

    this.addEnemy3 = function() {
        var enemy = this.enemy3.getFirstDead();
        if( (playmode == 1 && (score < 600 || this.boss1_exist == true || this.boss2_exist == true || this.boss2_die == true)) ||
            (playmode == 2 && (score + score2 < 600 || this.boss1_exist == true || this.boss2_exist == true || this.boss2_die == true))
        ) return;
        enemy.life = 3;
        enemy.anchor.setTo(0.5, 1);
        var x = game.rnd.between(30, 470);
        var x2 = game.rnd.between(30, 470);
        enemy.reset(x, 0);
        game.add.tween(enemy).to({x: x2, y: game.height/2 + 100}, 1000, Phaser.Easing.Sinusoidal.InOut)
                             .to({x: game.width - x, y: 50}, 1000, Phaser.Easing.Sinusoidal.InOut).start();
        enemy.body.velocity.y = 70;
    };

    this.addwind = function() {
        var wind = this.wind.getFirstDead();
        if( (playmode == 1 && (score < 600)) ||
            (playmode == 2 && (score + score2 < 600))
        ) return;
        wind.anchor.setTo(0.5, 1);
        wind.reset(game.width/2, 0);
        game.add.tween(wind).to({y: 1000}, 8000, Phaser.Easing.Sinusoidal.InOut).start();
    };

    this.Fire = function() {

        var bullet = this.mybu.getFirstDead();
        if(bullet) {
            bullet.from = 1;
            bullet.anchor.setTo(0.5, 0);
            bullet.reset(this.player.x, this.player.y - 30);
            bullet.body.velocity.y = -450;
            
            if(this.player.level >= 2) {
                bullet = this.mybu.getFirstDead();
                bullet.from = 1;
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(this.player.x, this.player.y - 30);
                bullet.body.velocity.y = -450;
                bullet.body.velocity.x = -30;
                
                bullet = this.mybu.getFirstDead();
                bullet.from = 1;
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(this.player.x, this.player.y - 30);
                bullet.body.velocity.y = -450;
                bullet.body.velocity.x = 30;
            }

            if(this.player.level >= 4) {
                bullet = this.mybu.getFirstDead();
                bullet.from = 1;
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(this.player.x, this.player.y - 30);
                bullet.body.velocity.y = -450;
                bullet.body.velocity.x = -60;
                
                bullet = this.mybu.getFirstDead();
                bullet.from = 1;
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(this.player.x, this.player.y - 30);
                bullet.body.velocity.y = -450;
                bullet.body.velocity.x = 60;
            }
        }
        this.shootsound.play();
    };

    this.Fire2 = function() {

        var bullet = this.mybu2.getFirstDead();
        if(bullet) {
            bullet.from = 2;
            bullet.anchor.setTo(0.5, 0);
            bullet.reset(this.player2.x, this.player2.y - 30);
            bullet.body.velocity.y = -450;
            
            if(this.player2.level >= 2) {
                bullet = this.mybu2.getFirstDead();
                bullet.from = 2;
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(this.player2.x, this.player2.y - 30);
                bullet.body.velocity.y = -450;
                bullet.body.velocity.x = -30;
                
                bullet = this.mybu2.getFirstDead();
                bullet.from = 2;
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(this.player2.x, this.player2.y - 30);
                bullet.body.velocity.y = -450;
                bullet.body.velocity.x = 30;
            }

            if(this.player2.level >= 4) {
                bullet = this.mybu2.getFirstDead();
                bullet.from = 2;
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(this.player2.x, this.player2.y - 30);
                bullet.body.velocity.y = -450;
                bullet.body.velocity.x = -60;
                
                bullet = this.mybu2.getFirstDead();
                bullet.from = 2;
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(this.player2.x, this.player2.y - 30);
                bullet.body.velocity.y = -450;
                bullet.body.velocity.x = 60;
            }
        }
        this.shootsound.play();
    };

    this.helperFire = function() {
        this.helper.forEachExists(function(helper) {
            var bullet = this.mybu.getFirstDead();
            bullet.from = 1;
            bullet.anchor.setTo(0.5, 0);
            if(game.time.now > (helper.bulletTime || 0)) {
                bullet.reset(helper.x, helper.y);
                bullet.body.velocity.y = -250;
                helper.bulletTime = game.time.now + 500;
            }
        }, this);
    };

    this.enemy1Fire = function() {
        this.enemy1.forEachExists(function(enemy) {
            var bullet = this.e1bu.getFirstDead();
            bullet.anchor.setTo(0.5, 0);
            if(game.time.now > (enemy.bulletTime || 0)) {
                bullet.reset(enemy.x - 0.5, enemy.y);
                bullet.body.velocity.y = 250;
                enemy.bulletTime = game.time.now + 1200;
            }
        }, this);
    };

    this.enemy2Fire = function() {
        this.enemy2.forEachExists(function(enemy) {
            if(game.time.now > (enemy.bulletTime || 0)) {
                var bullet = this.e2bu.getFirstDead();
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(enemy.x - 0.5, enemy.y);
                bullet.body.velocity.y = 100;

                bullet = this.e2bu.getFirstDead();
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(enemy.x - 0.5, enemy.y);
                bullet.body.velocity.y = 100;
                bullet.body.velocity.x = 30;

                bullet = this.e2bu.getFirstDead();
                bullet.anchor.setTo(0.5, 0);
                bullet.reset(enemy.x - 0.5, enemy.y);
                bullet.body.velocity.y = 100;
                bullet.body.velocity.x = -30;
                
                enemy.bulletTime = game.time.now + 1200;
            }
        }, this);
    };

    this.enemy3Fire = function() {
        this.enemy3.forEachExists(function(enemy) {
            var bullet = this.e3bu.getFirstDead();
            bullet.anchor.setTo(0.5, 0);
            if(game.time.now > (enemy.bulletTime || 0)) {
                bullet.reset(enemy.x - 0.5, enemy.y);
                bullet.body.velocity.y = 250;
                enemy.bulletTime = game.time.now + 800;
            }
        }, this);
    }

    this.boss1Fire = function() {
        var bullet = this.b1bu.getFirstDead();
        if(!bullet || this.boss1_exist == false || this.boss1_die == true) return;
        bullet.anchor.setTo(0.5, 0);
        bullet.reset(this.boss1.x, this.boss1.y + 60);
        bullet.body.velocity.y = 250;

        bullet = this.b1bu.getFirstDead();
        bullet.anchor.setTo(0.5, 0);
        bullet.reset(this.boss1.x, this.boss1.y + 60);
        bullet.body.velocity.y = 250;
        bullet.body.velocity.x = 80;

        bullet = this.b1bu.getFirstDead();
        bullet.anchor.setTo(0.5, 0);
        bullet.reset(this.boss1.x, this.boss1.y + 60);
        bullet.body.velocity.y = 250;
        bullet.body.velocity.x = -80;

        bullet = this.b1bu.getFirstDead();
        bullet.anchor.setTo(0.5, 0);
        bullet.reset(this.boss1.x, this.boss1.y + 60);
        bullet.body.velocity.y = 250;
        bullet.body.velocity.x = 180;

        bullet = this.b1bu.getFirstDead();
        bullet.anchor.setTo(0.5, 0);
        bullet.reset(this.boss1.x, this.boss1.y + 60);
        bullet.body.velocity.y = 250;
        bullet.body.velocity.x = -180;
    };

    this.boss2Fire = function() {

        var pick = game.rnd.pick([0,1,2]);
        if(pick == 0) var bullet = this.e1bu.getFirstDead();
        if(pick == 1) var bullet = this.e2bu.getFirstDead();
        if(pick == 2) var bullet = this.e3bu.getFirstDead();
        
        if(!bullet || this.boss2_exist == false || this.boss2_die == true) return;
        bullet.anchor.setTo(0.5, 0);
        bullet.reset(this.boss2.x, this.boss2.y + 60);
        bullet.body.velocity.y = game.rnd.between(250, 320);
        bullet.body.velocity.x = game.rnd.between(-120, 120);
    };

    this.clean = function() {

        if(this.skillnum == 0) return;
        this.skillnum --;
        this.skillLabel.setText("Skill:  " + this.skillnum);
        game.camera.flash(0x00ff00, 500);
        
        this.enemy1.forEachExists(function(enemy) {
            this.Edie_emitter.x = enemy.x;
            this.Edie_emitter.y = enemy.y;
            this.Edie_emitter.start(true, 500, null, 2);
            enemy.kill();
            score += 10;
            this.scoreLabel.setText("Score: " + score);
            this.bombsound.play();
        }, this);
        this.enemy2.forEachExists(function(enemy) {
            this.Edie_emitter.x = enemy.x;
            this.Edie_emitter.y = enemy.y;
            this.Edie_emitter.start(true, 500, null, 2);
            enemy.kill();
            score += 20;
            this.scoreLabel.setText("Score: " + score);
            this.bombsound.play();
        }, this);
        this.enemy3.forEachExists(function(enemy) {
            this.Edie_emitter.x = enemy.x;
            this.Edie_emitter.y = enemy.y;
            this.Edie_emitter.start(true, 500, null, 2);
            enemy.kill();
            score += 20;
            this.scoreLabel.setText("Score: " + score);
            this.bombsound.play();
        }, this);
        this.e1bu.forEachExists(function(bullet) {
            bullet.kill();
        }, this);
        this.e2bu.forEachExists(function(bullet) {
            bullet.kill();
        }, this);
        this.e3bu.forEachExists(function(bullet) {
            bullet.kill();
        }, this);
        this.b1bu.forEachExists(function(bullet) {
            bullet.kill();
        }, this);
    };

    this.pause = function() {
        
        if(this.pause_exist == false) {
            this.pause_exist = true;
            game.paused = true;
            game.input.onDown.add(this.unpause, this);
            this.menu = game.add.sprite(game.width/2, game.height/2, 'menu');
            this.menu.anchor.setTo(0.5, 0.5);
        } else {
            this.pause_exist = false;
            if(this.menu) this.menu.kill();
            game.paused = false;
        }
    };
    
    this.unpause = function(event) {
        
        var x1 = 87, x2 = 171, y1 = 265, y2 = 321;
        if((event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2)) {
            this.pause_exist = false;
            if(this.menu) this.menu.kill();
            game.paused = false;
        }

        var x1 = 207, x2 = 290, y1 = 265, y2 = 321;
        if((event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2)) {
            this.pause_exist = false;
            if(this.menu) this.menu.kill();
            game.paused = false;
            
            score = 0;
            score2 = 0;
            if(this.boss1_exist == true || this.boss2_exist == true) this.bossbgm.stop();
            else this.playbgm.stop();     
            game.state.start('main');
        }

        var x1 = 328, x2 = 411, y1 = 265, y2 = 321;
        if((event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2)) {
            this.pause_exist = false;
            if(this.menu) this.menu.kill();
            game.paused = false;
            score = 0;
            score2 = 0;
            if(this.boss1_exist == true || this.boss2_exist == true) this.bossbgm.stop();
            else this.playbgm.stop();     
            game.state.start('menu');
        }
    };

    this.addgift = function() {
        var newgift = this.gift.getFirstDead();
        if(!newgift || this.boss2_die == true) return;
        newgift.reset(game.rnd.between(30, 470), 0);
        newgift.body.velocity.y = 100;
        newgift.body.velocity.x = 100 * game.rnd.pick([1,-1]);
        newgift.body.bounce.x = 1;
        newgift.body.bounce.y = 1;
        newgift.body.collideWorldBounds = true;
        newgift.mode = game.rnd.pick([0,1,2]);
        if(playmode == 2) newgift.mode = 0;
    };

    this.levelup = function(player, gift) {
        gift.kill();
        this.gift_exist = false;
        player.heart ++;

        if(gift.mode == 0) {

            if(playmode == 1) {
                if(player.level < 4) player.level ++;
                else this.skillnum ++;
            } else player.level ++;
        
            if(player.color == 1) this.lifeLabel.setText("Life:  " + this.player.heart);
            else this.lifeLabel2.setText("Life:  " + this.player2.heart + "   (2P)");
        } else if(gift.mode == 1) {

            this.lifeLabel.setText("Life:  " + this.player.heart);
            var helper = this.helper.getFirstDead();
            helper.shield = this.shield.getFirstDead();
            helper.anchor.setTo(0.5, 0);
            helper.shield.anchor.setTo(0.5, 0);
            helper.reset(game.rnd.between(30, 140), 600);
            helper.shield.reset(helper.x, 595);
            helper.body.velocity.y = -30;
            helper.shield.body.velocity.y = -30;

            helper = this.helper.getFirstDead();
            helper.shield = this.shield.getFirstDead();
            helper.anchor.setTo(0.5, 0);
            helper.shield.anchor.setTo(0.5, 0);
            helper.reset(game.rnd.between(140, 250), 600);
            helper.shield.reset(helper.x, 595);
            helper.body.velocity.y = -30;
            helper.shield.body.velocity.y = -30;

            helper = this.helper.getFirstDead();
            helper.shield = this.shield.getFirstDead();
            helper.anchor.setTo(0.5, 0);
            helper.shield.anchor.setTo(0.5, 0);
            helper.reset(game.rnd.between(250, 360), 600);
            helper.shield.reset(helper.x, 595);
            helper.body.velocity.y = -30;
            helper.shield.body.velocity.y = -30;

            helper = this.helper.getFirstDead();
            helper.shield = this.shield.getFirstDead();
            helper.anchor.setTo(0.5, 0);
            helper.shield.anchor.setTo(0.5, 0);
            helper.reset(game.rnd.between(360, 470), 600);
            helper.shield.reset(helper.x, 595);
            helper.body.velocity.y = -30;
            helper.shield.body.velocity.y = -30;
        } else if(gift.mode == 2) {

            this.lifeLabel.setText("Life:  " + this.player.heart);
            game.time.events.remove(this.genBullet);
            game.time.events.repeat(200, 40, this.autoaiming, this);
            game.time.events.add(8000, this.resubullet, this);
        }

        this.levelupsound.play();
    };

    this.autoaiming = function() {

        var bullet = this.mybu.getFirstDead();
        bullet.anchor.setTo(0.5, 0);
        bullet.reset(this.player.x, this.player.y - 30);
        bullet.body.velocity.y = -450;

        this.enemy1.forEachExists(function(enemy) {
            var bullet = this.mybu.getFirstDead();
            bullet.anchor.setTo(0.5, 0);
            bullet.reset(this.player.x, this.player.y - 30);
            var tween = game.add.tween(bullet).to({x: enemy.x, y: enemy.y}, 1000).start();
            tween.onComplete.add(this.freebullet, this);
        }, this);

        this.enemy2.forEachExists(function(enemy) {
            var bullet = this.mybu.getFirstDead();
            bullet.anchor.setTo(0.5, 0);
            bullet.reset(this.player.x, this.player.y - 30);
            var tween = game.add.tween(bullet).to({x: enemy.x, y: enemy.y}, 1000).start();
            tween.onComplete.add(this.freebullet, this);
        }, this);

        this.enemy3.forEachExists(function(enemy) {
            var bullet = this.mybu.getFirstDead();
            bullet.anchor.setTo(0.5, 0);
            bullet.reset(this.player.x, this.player.y - 30);
            var tween = game.add.tween(bullet).to({x: enemy.x, y: enemy.y}, 1000).start();
            tween.onComplete.add(this.freebullet, this);
        }, this);
        
        this.shootsound.play();
    };
    this.freebullet = function(bullet) { bullet.kill();};
    this.resubullet = function() { this.genBullet = game.time.events.loop(200, this.Fire, this);};
    this.slow = function() { this.inwind = true;};

    this.exit = function() {
        if(playmode == 1) {
            this.bgm.stop();     
            game.state.start('over');
        } else {
            this.bgm.stop();     
            game.state.start('over2');
        }
    };
}

game.States.over = function() {
    this.create = function() {

        // write database
        var name = prompt("Your name", "name");
        if(name != null){
            var database = firebase.database().ref('/scoreboard');
            database.push().set({
                score: score,
                order: -score,
                name: name
            });
        }
        yourscore = game.add.text(game.width/2, 80, 'Your score    ' + score, {font: '30px Arial', fill: '#ffffff'});
        yourscore.anchor.setTo(0.5, 0.5);

        // read database
        Leaderboard = game.add.text(game.width/2, 150, 'Leaderboard', {font: '30px Arial', fill: '#ffffff'});
        Leaderboard.anchor.setTo(0.5, 0);
        
        var database = firebase.database().ref('/scoreboard').orderByChild('order').limitToFirst(5);
        database.once('value', function(snapshot){
            var nameText  = '';
            var scoreText = '';
            snapshot.forEach(function(childSnapshot){
                nameText  += childSnapshot.val().name + '\n';
                scoreText += childSnapshot.val().score + '\n';
            });
            nameLabel = game.add.text(game.width/2 - 50, 200, nameText, {font: '30px Arial', fill: '#ffffff'});
            nameLabel.anchor.setTo(0.5, 0);

            scoreLabel = game.add.text(game.width/2 + 50, 200, scoreText, {font: '30px Arial', fill: '#ffffff'});
            scoreLabel.anchor.setTo(0.5, 0);
        });

        // replay
        replaybutton = game.add.button(game.width/2, 480, 'replaybutton', this.Click);
        replaybutton.anchor.setTo(0.5, 0.5);
    }
    // Click 'replaybutton' to change the state
    this.Click = function() {
        score = 0;
        score2 = 0;
        game.state.start('menu');
    };
}

game.States.over2 = function() {
    this.create = function() {

        if(score > score2) {
            win = game.add.text(game.width/2, 80, '1P    WIN ! ! !', {font: '30px Arial', fill: '#ffffff'});
            win.anchor.setTo(0.5, 0.5);
        } else if(score < score2) {
            win = game.add.text(game.width/2, 80, '2P    WIN ! ! !', {font: '30px Arial', fill: '#ffffff'});
            win.anchor.setTo(0.5, 0.5);
        } else {
            win = game.add.text(game.width/2, 80, 'Draw ! ! !', {font: '30px Arial', fill: '#ffffff'});
            win.anchor.setTo(0.5, 0.5);
        }
        onePscore = game.add.text(game.width/2 - 100, 300, '1P score    ' + score, {font: '30px Arial', fill: '#ffffff'});
        twoPscore = game.add.text(game.width/2 - 100, 380, '2P score    ' + score2, {font: '30px Arial', fill: '#ffffff'});
        onePscore.anchor.setTo(0, 0.5);
        twoPscore.anchor.setTo(0, 0.5);

        // replay
        replaybutton = game.add.button(game.width/2, 480, 'replaybutton', this.Click);
        replaybutton.anchor.setTo(0.5, 0.5);
    }
    // Click 'replaybutton' to change the state
    this.Click = function() {
        score = 0;
        score2 = 0;
        game.state.start('menu');
    };
}

game.state.add('boot', game.States.boot);
game.state.add('load', game.States.load);
game.state.add('menu', game.States.menu);
game.state.add('main', game.States.main);
game.state.add('over', game.States.over);
game.state.add('over2', game.States.over2);
game.state.start('boot');